This is a API written in .Net Core to support penny pig android application.

The key features of this API are,
1. Connects to mongoDB to retrieve data.
2. Uses JSON soft to return the data in json format to android application.

Note: We should configure the server befor deploying the API.